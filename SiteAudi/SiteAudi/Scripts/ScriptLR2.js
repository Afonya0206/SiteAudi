﻿var param;
var i;
var inimg = -1;

$(document).ready(function () {
    ToHideSlider();
    ShowSlider();
});

function ToHideSlider() {
    for (i = 0; i < 3; i++) {
        param = "";
        param = "#" + i;
        $(param).fadeOut(0);
    }
}

function ShowSlider()
{
    inimg++;
    var prew = inimg - 1;
    param = "#" + prew;
    $(param).fadeOut(0);
    if (inimg == 3)
        inimg = 0;
    param = "";
    param = "#" + inimg;
    $(param).fadeIn(1500);
    setTimeout(ShowSlider, 4000);
}