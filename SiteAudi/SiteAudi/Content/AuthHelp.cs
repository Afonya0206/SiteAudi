﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SiteAudi.Models;

namespace SiteAudi.Content
{
    public static class AuthHelp
    {
        public static User GetUserById(int id)
        {
            DataBaseContext dbContext = new DataBaseContext();
            return dbContext.Users.FirstOrDefault(u => u.Id == id);
        }

        public static void LogOffUser(HttpContextBase httpContext)
        {
            if (httpContext.Request.Cookies["AuthId"] != null)
            {
                var CookieId = new HttpCookie("AuthId")
                {
                    Expires = DateTime.Now.AddDays(-1),
                };
                httpContext.Response.Cookies.Add(CookieId);
            }
        }
    }
}