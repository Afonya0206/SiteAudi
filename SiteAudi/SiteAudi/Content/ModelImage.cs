﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;

namespace SiteAudi.Content
{
    public class ModelImage
    {
        List<string> images;

        public ModelImage()
        {
            images = new List<string>();
        }

        public List<string> IMAGES
        {
            set
            {
                images = value;
            }
            get
            {
                return images;
            }
        }
    }
}