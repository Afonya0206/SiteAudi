﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SiteAudi.Models
{
    public class DataBaseContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<OrderCar> OrderCars { get; set; }
        public DbSet<Accessory> Accessories { get; set; }
        public DbSet<User> Users { get; set; }
    }
}