﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteAudi.Models
{
    public class Category
    {
        // ID категории
        public int Id { get; set; }

        // название
        public string NameCategory { get; set; }

        // описание
        public string DescriptionCategory { get; set; }
    }
}