﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteAudi.Models
{
    public class Accessory
    {
        // ID аксессуара
        public int Id { get; set; }

        // наименование
        public string NameAccessory { get; set; }

        // описание
        public string DescriptionAccessory { get; set; }

        // путь к картинке
        public string ImagePathAccessory { get; set; }
    }
}