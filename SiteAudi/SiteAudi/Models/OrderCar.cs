﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteAudi.Models
{
    public class OrderCar
    {
        // ID заказа
        public int Id { get; set; }

        [Required]
        [Display(Name = "Имя")]
        // имя заказчика
        public string NameCustomer { get; set; }

        [Display(Name = "Email")]
        [Required]
        [DataType(DataType.EmailAddress)]
        // email заказчика
        public string EmailCustomer { get; set; }

        [Required]
        [Display(Name = "Телефон")]
        // телефон заказчика
        public string PhoneCustomer { get; set; }

        // id заказанной машины
        public int IdCar { get; set; }
    }
}