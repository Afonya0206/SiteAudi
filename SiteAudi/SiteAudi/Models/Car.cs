﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace SiteAudi.Models
{
    public class Car
    {
        // ID машины
        public int Id { get; set; }

        // модель
        public string ModelCar { get; set; }

        // описание
        public string DescriptionCar { get; set; }

        // цена
        public int PriceCar { get; set; }

        // путь к картинке
        public string ImagePathCar { get; set; }

        // категория (внешний ключ)
        public int CategoryId { get; set; }

        // навигационное свойство
        public Category Category { get; set; }
    }
}