﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SiteAudi.Models;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace SiteAudi.Controllers
{
    public class CarController : Controller
    {
        DataBaseContext dbcontext = new DataBaseContext();
        //
        // GET: /Car/

        public ActionResult Index(bool? Cars, bool? SUV)
        {
            var cars = dbcontext.Cars.Include(category => category.Category);
            if (Cars == SUV == true)
            {
                return View(cars);
            }
            else
            {
                if (Cars == true)
                {
                    cars = cars.Where(c => c.CategoryId == 1);
                    return View(cars); 
                }
                else
                {
                    if (SUV == true)
                    {
                        cars = cars.Where(c => c.CategoryId == 3);
                        return View(cars);
                    }
                    else
                        return View(cars);
                }
            }  
        }
        protected override void Dispose(bool disposing)
        {
            dbcontext.Dispose();
            base.Dispose(disposing);
        }
    }
}
