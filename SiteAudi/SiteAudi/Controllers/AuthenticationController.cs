﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SiteAudi.Models;
using SiteAudi.Content;

namespace SiteAudi.Controllers
{
    public class AuthenticationController : Controller
    {
        DataBaseContext dbContext = new DataBaseContext();
        //
        // GET: /Authentication/

        [HttpPost]
        public ActionResult Index()
        {
            var users = dbContext.Users;
            String login = Request.Params["login"];
            String pwd = Request.Params["pwd"];
            if(login!=""&&pwd!="")
            {
                User user1 = users.FirstOrDefault(u => u.Email == login);
                if (user1 == null)
                    return RedirectToAction("Index", "Registration");
                else
                {
                    user1 = users.FirstOrDefault(u => u.Password == pwd);
                    if (user1 == null)
                    {
                        TempData["ErrorPassword"] = "Неверный пароль";
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        var CookieId = new HttpCookie("AuthId")
                        {
                            Value = user1.Id.ToString(),
                            Expires = DateTime.Now.AddHours(3),
                        };
                        HttpContext.Response.Cookies.Add(CookieId);
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOff()
        {
            AuthHelp.LogOffUser(HttpContext);
            return RedirectToAction("Index", "Home");
        }
    }
}
