﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SiteAudi.Models;

namespace SiteAudi.Controllers
{
    public class RegistrationController : Controller
    {
        DataBaseContext dbcontext = new DataBaseContext();
        //
        // GET: /Registration/

        public ActionResult Index()
        {
            return View();
        }
    
        [HttpPost]
        public ActionResult Index(User user)
        {
            if(ModelState.IsValid)
            {
                string email = user.Email;
                User user1 = dbcontext.Users.FirstOrDefault(u => u.Email == email);
                if(user1==null)
                {
                    if (user.Password == Request.Params["ConfirmPassword"])
                    {
                        dbcontext.Users.Add(user);
                        dbcontext.SaveChanges();
                        return View("~/Views/Registration/ResultRegistration.cshtml");
                    }
                    else
                        ModelState.AddModelError("Password", "ПАРОЛИ НЕ СОВПАДАЮТ.");
                }
                else
                    ModelState.AddModelError("Email", "Пользователь с таким Email уже есть.");
            }
            return View();
        }
    }
}
