﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SiteAudi.Content;

namespace SiteAudi.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            ModelImage modelimage = new ModelImage();
            modelimage.IMAGES.AddRange(System.IO.Directory.GetFiles(@"D:\Универ\Web-технологии\4 курс\ЛР2\SiteAudi\SiteAudi\Images\SlideShowImages"));
            for (int i = 0; i < modelimage.IMAGES.Count; i++)
            {
                modelimage.IMAGES[i] = modelimage.IMAGES[i].Replace(@"D:\Универ\Web-технологии\4 курс\ЛР2\SiteAudi\SiteAudi", "~");
                modelimage.IMAGES[i] = modelimage.IMAGES[i].Replace(@"\", "/");
            } 
            return View(modelimage);
        }

    }
}
