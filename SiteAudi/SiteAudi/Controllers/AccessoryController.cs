﻿using SiteAudi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteAudi.Controllers
{
    public class AccessoryController : Controller
    {
        DataBaseContext dbcontext = new DataBaseContext();
        //
        // GET: /Accessory/

        public ActionResult Index()
        {
            return View(dbcontext.Accessories);
        }

    }
}
