﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SiteAudi.Models;

namespace SiteAudi.Controllers
{
    public class OrderCarController : Controller
    {
        DataBaseContext dbContext = new DataBaseContext();
        //
        // GET: /Order/

        [HttpGet]
        public ActionResult Index(int IdOrderCar)
        {
            if (HttpContext.Request.Cookies["AuthId"] != null)
            {
                ViewBag.IdOrderCar = IdOrderCar;
                return View();
            }
            else
            {
                return View("~/Views/OrderCar/AuthenticationForOrderCar.cshtml");
            }
        }

        [HttpPost]
        public ActionResult Index(OrderCar ordercar)
        {
            if(ModelState.IsValid)
            {
                dbContext.OrderCars.Add(ordercar);
                dbContext.SaveChanges();
                return View("~/Views/OrderCar/ResultOrderCar.cshtml");
            }
            ViewBag.IdOrderCar = ordercar.IdCar;
            return View("~/Views/OrderCar/Index.cshtml");
        }
    }
}
